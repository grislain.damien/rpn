<?php

declare(strict_types=1);

namespace Ulco\Tests;

use Ulco\Rpn;
use PHPUnit\Framework\TestCase;

class RpnTest extends TestCase
{
    private ?Rpn $rpn = null;

    protected function setUp(): void
    {
        parent::setUp();

        $this->rpn = new Rpn();
    }

    public function testCalc1(): void
    {
        self::assertEquals(8, $this->rpn->calc('5 3 +'));
    }

    public function testCalc2(): void
    {
        self::assertEquals(3, $this->rpn->calc('6 2 /'));
    }

    public function testCalc3(): void
    {
        self::assertEquals(10, $this->rpn->calc('5 2 - 7 +'));
    }

    public function testCalc4(): void
    {
        self::assertEquals(10, $this->rpn->calc('7 5 2 - +'));
    }

    public function testCalc5(): void
    {
        self::assertEquals(141, $this->rpn->calc('3 5 8 x 7 + x'));
    }
}
