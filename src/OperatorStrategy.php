<?php

namespace Ulco;

interface OperatorStrategy
{
    public function execute(int $firstValue, int $secondValue): int;

}