<?php

namespace Ulco;

class ConcreteStrategyMultiply implements \Ulco\OperatorStrategy
{
    public function execute(int $firstValue, int $secondValue): int
    {
        return $firstValue * $secondValue;
    }
}