<?php

namespace Ulco;

class ConcreteStrategyDivisible implements \Ulco\OperatorStrategy
{
    public function execute(int $firstValue, int $secondValue): int
    {
        return $firstValue / $secondValue;
    }
}