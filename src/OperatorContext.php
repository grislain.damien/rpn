<?php

namespace Ulco;

class OperatorContext
{
    private OperatorStrategy $operatorStrategy;

    /**
     * @param OperatorStrategy $operatorStrategy
     */
    public function setOperatorStrategy(OperatorStrategy $operatorStrategy): void
    {
        $this->operatorStrategy = $operatorStrategy;
    }

    public function executeStrategy(int $firstValue, int $secondValue): int
    {
        return $this->operatorStrategy->execute($firstValue, $secondValue);
    }

}