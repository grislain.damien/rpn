<?php

namespace Ulco;

class ConcreteStrategyAdd implements OperatorStrategy
{
    public function execute(int $firstValue, int $secondValue): int
    {
        return $firstValue + $secondValue;
    }
}