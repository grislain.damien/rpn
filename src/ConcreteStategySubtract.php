<?php

namespace Ulco;

class ConcreteStategySubtract implements \Ulco\OperatorStrategy
{
    public function execute(int $firstValue, int $secondValue): int
    {
        return $firstValue - $secondValue;
    }
}