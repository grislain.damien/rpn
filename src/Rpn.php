<?php

declare(strict_types=1);

namespace Ulco;

use function PHPUnit\Framework\matches;

class Rpn
{
    public function calc(string $stringCalc): int
    {
        $operatorContext = new OperatorContext();
        $values = explode(' ', $stringCalc);
        $stack = [];

        foreach ($values as $value) {
            if (is_numeric($value)) {
                $stack[] = $value;
            } else {
                $secondNumber = (int)array_pop($stack);
                $firstNumber = (int)array_pop($stack);

                if ($value === '+') {
                    $operatorContext->setOperatorStrategy(new ConcreteStrategyAdd());
                } elseif ($value === '/') {
                    $operatorContext->setOperatorStrategy(new ConcreteStrategyDivisible());
                } elseif ($value === '-') {
                    $operatorContext->setOperatorStrategy(new ConcreteStategySubtract());
                } elseif ($value === 'x') {
                    $operatorContext->setOperatorStrategy(new ConcreteStrategyMultiply());
                }

                $stack[] = $operatorContext->executeStrategy($firstNumber, $secondNumber);
            }
        }

        return $stack[0];
    }
}
